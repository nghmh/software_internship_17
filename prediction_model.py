import pandas as pd
import numpy as np
from pandas import Series, DataFrame
import os
import matplotlib.pylab as plt
from sklearn.cross_validation import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report
import sklearn.metrics
from sklearn.model_selection import cross_val_score
# Feature Importance
from sklearn import datasets
from sklearn.ensemble import ExtraTreesClassifier
# Build model on training data
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import BernoulliNB
from sklearn.svm import LinearSVC

class DataDescriptor(object):
    def __init__(self, data, header):
        self.data = data
        self.header = header
        self.predictors = self.data[self.header + ["wikiID", "neClass"]]
        self.target = self.data.neClass


def readFeatures():
    STATISTICS_FILE="datafeature_statistics.csv"
    properties = {}
    with open(STATISTICS_FILE, 'r') as f:
        for l in f.readlines()[1:]:
            properties[l.strip().split(",")[0]] = np.bool

    data = pd.read_csv("complete_features.csv", dtype=properties)
    header = list(data)[5: ]
    return DataDescriptor(data, header)

def printScores(predictions, tar_test):
    # shows the correct and incorrect classification
    print 'confusion matrix:'
    print sklearn.metrics.confusion_matrix(tar_test, predictions, labels=["ACT","NONE","LOC","ORG"])
    # the percent of the data that has been predicted correctly by model
    print 'accuracy score:'
    print sklearn.metrics.accuracy_score(tar_test, predictions)

def extraTreeFeatureImportance(descriptor, pred_train, tar_train, file_name):
    # fit an Extra Trees model to the data
    model = ExtraTreesClassifier()
    model.fit(pred_train, tar_train)
    # display the relative importance of each attribute
    print "relative importance of attributes:"
    print(model.feature_importances_)

    #f.write("Feature, Importance\n")
    #add properties to the file
    stacked = np.column_stack((descriptor.header, model.feature_importances_))
    np.savetxt(file_name, fmt='%s',X=stacked,delimiter=',', header="Feature,Importance" )
    return stacked

#Running different number of trees to tune the estimator parameter for 
#random forests classifier 
def calculateTreeAccuracy(trees, pred_train, tar_train, pred_test, tar_test):
    accuracy = np.zeros(trees)

    for idx in range(trees):
        print 'random tree {} of {}'.format(idx,trees)
        predictions = randomForest(pred_train, tar_train, pred_test, idx+5)
        accuracy[idx] = sklearn.metrics.accuracy_score(tar_test, predictions)

    return accuracy

#Split data set. keep only the top 'feature_count' properties by occurance as predictors.
def splitDataSet(descriptor, feature_count, feature_importance):
    # get predictors
    predictor_names = []
    for idx,r in feature_importance.iterrows():
        feature_count -= 1
        if feature_count == 0:
            break
        if r['property'] is not None:
            predictor_names.append(r['property'])
    predictors=descriptor.data[predictor_names]
    return train_test_split(predictors, descriptor.target, test_size=.4)

#Run a series of tests on a subset of features measuring the performance of the classifiers.
def classifierSeries(descriptor, feature_importance, file_name, classifier):
    print "Running measurements for classifier '{}'".format(classifier_name)
    with open(file_name, "w") as f:
        f.write("NoProperties,Precision,Recall,FScore\n")
        for idx in [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]:
            pred_train, pred_test, tar_train, tar_test = splitDataSet(descriptor, idx, feature_importance)
            classifier.fit(pred_train, tar_train)
            predictions = classifier.predict(pred_test)
            precision = sklearn.metrics.precision_score(tar_test, predictions, average='macro')
            recall = sklearn.metrics.recall_score(tar_test, predictions,average='macro')
            fscore = sklearn.metrics.f1_score(tar_test, predictions,average='macro')
            f.write("{fcount},{precision},{recall},{fscore}\n".format(fcount=idx, precision=precision, recall=recall, fscore=fscore))

if __name__ == '__main__':
    d = readFeatures()

    #Explanatory Variables/independent variables: define the properties for prediction.
    predictors = d.predictors

    # response or target variable
    target = d.target

    feature_importance = pd.read_csv("datafeature_statistics.csv")
    feature_importance.sort_values(by='frequency', inplace=True, ascending=False)

    classifier_name  = [ "random_forest", "LinearSVC", "SGD", "naive_bayes" ]
    classifiers = [ RandomForestClassifier(n_estimators=30), LinearSVC(max_iter=1000), SGDClassifier(loss='hinge'), BernoulliNB()]

    for classifier,classifier_name in zip(classifiers, classifier_name):
        # Split into training and testing sets
        # split into 40% test data and 60% training data
        pred_train, pred_test, tar_train, tar_test = train_test_split(predictors, target, test_size=.4)
        # Print confusion matrix
        classifier.fit(pred_train[d.header], tar_train)
        prediction = classifier.predict(pred_test[d.header])

        # write misclassified values
        pred_test['predictions'] = prediction
        with open("{}_misclassified.csv".format(classifier_name), "w") as f:
            f.write("Prediction,Actual,wikiID\n")
            for idx,r in pred_test.iterrows():
                if r['neClass'] != r['predictions']:
                    f.write("{p},{a},{w}\n".format(p=r['predictions'], a=r['neClass'], w=r['wikiID']))

        # print accuracy
        printScores(prediction, tar_test)

        # cross validation: check the accuracy of model with a 10-fold cross validation
        scores = cross_val_score(classifier, predictors[d.header], target, cv=10)
        np.savetxt("{}_cross_validation_scores.csv".format(classifier_name), scores, delimiter=',')
        classifierSeries(d, feature_importance, "{}_results.csv".format(classifier_name), classifier)
