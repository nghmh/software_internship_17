import pandas as pd
import re
import os
import fileinput
import argparse

# global variable because we need to use it in a dataframe.apply function
property_map = {}

def processLine(line, relevantProperties):
    """ Append dense property to the end of the line. Order is defined by the property map (relevantProperties)"""
    local_properties = getPropertyList(line)
    line = re.sub(",\"?\[.*?\]\"?","",line)
    line = line.strip()
    line += ","
    addedProperties = []
    for p in relevantProperties:
       if p in local_properties:
           addedProperties.append("1")
       else:
           addedProperties.append("0")
    line += ",".join(addedProperties)
    return line

def buildHeader(header_line, properties):
    """ Build new CSV header; remove property string, which happens to be the third element"""
    head_list = header_line.strip().split(",")
    head_list.pop(3)
    head_list += properties
    return ",".join(head_list)


def getPropertyList(property_string):
    """ Parse the property string and return a list of properties """
    property_list = re.findall("\[(.*?)\]", property_string)
    if len(property_list) == 1:
        property_list = [p.strip() for p in property_list[0].split(",")]
        return property_list
    else:
        print "Invalid record: {}".format(row)
        return []

def getRelevantProperties(input_file, threshold):
    """ Read the input CSV into a dataframe, and make a map with the property occurances """
    property_map = {}

    # file name of the csv export of the output collection of merge.py
    dataframe = pd.read_csv(input_file)

    for index,row in dataframe.iterrows():
        property_list = getPropertyList(row['property'])
        for p in property_list:
            if p == '':
                p="None"
            if property_map.has_key(p):
                property_map[p] += 1
            else:
                property_map[p] = 1
    property_map = { k:v for k,v in property_map.items() if v > threshold }
    return sorted(property_map)

def makeDensePropertyCSV(input_file, output_file, property_map):
    """ Read input CSV again and for each line append the dense properties"""
    with open(output_file, 'w') as output:
        for line in fileinput.input(input_file):
            if fileinput.isfirstline():
                output.write("{}\n".format(buildHeader(line, property_map)))
            else:
                output.write("{}\n".format(processLine(line, property_map)))



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Make dense property matrix")
    parser.add_argument('--input_file','-i', help='Input CSV file', required=True)
    parser.add_argument('--output_file','-o', help='Output CSV file')
    parser.add_argument('--threshold','-t', help='Threshold for filtering low occuring properties', type=int, default=100)

    args = parser.parse_args()
    if args.output_file is None:
        args.output_file = '_output'.join(os.path.splitext(args.input_file))

    property_map = getRelevantProperties(args.input_file, args.threshold)
    makeDensePropertyCSV(args.input_file, args.output_file, property_map)
