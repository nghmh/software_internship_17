from pymongo import MongoClient
import argparse

from config import *

class DBConnector(object):
    @staticmethod
    def connect(host=MONGODB_HOST, port=MONGODB_PORT, authdb=MONGODB_AUTHENTICATION_DB, user=MONGODB_USER_NAME, password=MONGODB_PASSWORD):
        client = MongoClient(host, port)
        client[authdb].authenticate(user, password)
        return client

def mergeYagoWithWikidata(yagoCollection, wikidataCollection, outputCollection):
    client = DBConnector.connect()

    try:
        yago_collection = client[MONGODB_AUTHENTICATION_DB][yagoCollection]
        wikidata_collection = client[MONGODB_AUTHENTICATION_DB][wikidataCollection]

        output_collection = client[MONGODB_AUTHENTICATION_DB][outputCollection]

        # loop through yago collection and find matching wikidata entries
        # Explicitly set batch size to avoid the cursor being closed by mongodb
        yago_cursor = yago_collection.find().batch_size(2000)

        counter = 0
        for document in yago_cursor:
            counter += 1
            if counter % 100000 is 0:
                print "{} documents processed".format(str(counter))

            yagoID = document.get("Entity")
            # Yago entities match the wiki data URL links after small replacements
            wikidata_url = yagoID.replace('<','').replace('_',' ').replace('>','')
            wikidata_document = wikidata_collection.find_one({"sitelinks.enwiki.title": wikidata_url })

            if wikidata_document:
                # output merged dataset
                output_collection.insert_one({
                    "yagoID": yagoID,
                    "wikiID": wikidata_document.get("id"),
                    "class": document.get("class").replace("<", "").replace(">", "")
                })
    finally:
        client.close()


def buildNeClasses(mergedYagoCollection, outputCollection):
    client = DBConnector.connect()

    try:
        merged_collection = client[MONGODB_AUTHENTICATION_DB][mergedYagoCollection]

        output_collection = client[MONGODB_AUTHENTICATION_DB][outputCollection]

        # loop through merged yago collection and build neClasses
        # Explicitly set batch size to avoid the cursor being closed by mongodb
        # Sort results to enable lazy write backs to DB
        merged_cursor = merged_collection.find().sort("yagoID",1).batch_size(2000)
        lastDocument = None
        for document in merged_cursor:
            yago_class = document.get("class")
            yago_id = document.get("yagoID")
            wiki_id = document.get("wikiID")

            # if the document's class matches one of the following classes: "wordnet_social_group_107950920",
            # "wordnet_person_100007846" or "yagoGeoEntity" assigns the label "ORG", "ACT" or "LOC" to the
            # document respectively. Otherwise assigns label "NONE" to it.
            if yago_class == "wordnet_social_group_107950920":
                yago_class = "ORG"
            elif yago_class == "wordnet_person_100007846":
                yago_class = "ACT"
            elif yago_class == "yagoGeoEntity":
                yago_class = "LOC"
            else:
                yago_class = "NONE"


            if lastDocument is None:
                lastDocument = {
                    "yagoID":yago_id,
                    "wikiID": wiki_id,
                    "neClass": yago_class
                }


            if lastDocument["yagoID"] != yago_id:
                output_collection.insert_one(lastDocument)
                lastDocument = {
                    "yagoID":yago_id,
                    "wikiID": wiki_id,
                    "neClass": yago_class
                }
            else:
                # update neClass field of the buffered document if the current class is not None
                if yago_class != "NONE" and lastDocument["neClass"] == "NONE":
                    lastDocument["neClass"] = yago_class

    finally:
        client.close()


def buildWikidataFeatures(mergedYagoCollection, wikidataCollection, outputCollection):
    client = DBConnector.connect()

    try:
        merged_collection = client[MONGODB_AUTHENTICATION_DB][mergedYagoCollection]
        wikidata_collection = client[MONGODB_AUTHENTICATION_DB][wikidataCollection]
        output_collection = client[MONGODB_AUTHENTICATION_DB][outputCollection]

        merged_cursor = merged_collection.find().batch_size(2000)

        for document in merged_cursor:
            yago_class = document.get("neClass")
            wiki_Id = document.get("wikiID")
            yago_Id = document.get("yagoID")

            wikidata_document = wikidata_collection.find_one({"id": wiki_Id})
            # grab the feature from wikidata. We are only interested in the keys though.
            if wikidata_document:
                output_collection.insert_one({
                    "yagoID": yago_Id,
                    "wikiID": wiki_Id,
                    "property": wikidata_document.get("claims").keys(),
                    "neClass": yago_class
                })

    finally:
        client.close()


def merge(yagoCollection, wikidataCollection, outputCollection):
    yagoMergedCollection = "{}_MERGED".format(yagoCollection)
    yagoMergedClassifiedCollection = "{}_CLASSIFIED".format(yagoMergedCollection)
    mergeYagoWithWikidata(yagoCollection, wikidataCollection, yagoMergedCollection)
    buildNeClasses(yagoMergedCollection, yagoMergedClassifiedCollection)
    buildWikidataFeatures(yagoMergedClassifiedCollection, wikidataCollection, outputCollection)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Merge the YAGO dataset with Wikidata Dataset')
    parser.add_argument('--yago','-y', help='YAGO collection', default='yagoclasses')
    parser.add_argument('--wiki','-w', help='Wikidata collection', default='wikidata_copy')
    parser.add_argument('--output', '-o', help='Output collection', required=True)

    args = parser.parse_args()

    merge(args.yago, args.wiki, args.output)
